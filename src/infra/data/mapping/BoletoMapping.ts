import { EntitySchema, EntitySchemaColumnOptions } from "typeorm";

import BoletoConciliacao from "@financeiro/conciliacao/entities/boleto/Boleto";
import BoletoFatura from "@financeiro/receber/entities/fatura/Boleto";

const BoletoBaseSchemaPart = {
  id: {
    type: Number,
    primary: true,
    generated: true,
  } as EntitySchemaColumnOptions,
};

export const BoletoConciliacaoSchema = new EntitySchema<BoletoConciliacao>({
  name: "boleto_conciliacao",
  tableName: "boleto",
  columns: {
    ...BoletoBaseSchemaPart,
    pago: {
      type: Number,
    },
  },
});

export const BoletoFaturaSchema = new EntitySchema<BoletoFatura>({
  name: "boleto_fatura",
  tableName: "boleto",
  columns: {
    ...BoletoBaseSchemaPart,
    numeracao: {
      type: Number,
    },
  },
});

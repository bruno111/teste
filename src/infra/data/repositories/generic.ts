import { getRepository, Repository } from "typeorm";

class GenericRepository<T> {
  private repository: Repository<T>;

  constructor(repo: { new (): T }) {
    this.repository = getRepository(repo);
  }

  public async list(): Promise<T[]> {
    return await this.repository.find();
  }
}

export default GenericRepository;

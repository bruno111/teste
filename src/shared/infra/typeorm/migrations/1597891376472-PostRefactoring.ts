import {MigrationInterface, QueryRunner} from "typeorm";

export class PostRefactoring1597891376472 implements MigrationInterface {
    name = 'PostRefactoring1597891376472'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "boleto" ("id" SERIAL NOT NULL, "pago" boolean NOT NULL, "numeracao" integer NOT NULL, CONSTRAINT "PK_b95cb77c026b2963089f016b91b" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "boleto"`);
    }

}

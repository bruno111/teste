interface IBoleto {
  id: number;
}

class Fatura {
  id: number;
  boleto: IBoleto;
}

export default Fatura;

import GenericRepository from "infra/data/repositories/generic";

class GenericAppService<T> {
  private repository: GenericRepository<T>;

  constructor(entity: { new (): T }) {
    this.repository = new GenericRepository(entity);
  }

  public async list(): Promise<T[]> {
    return await this.repository.list();
  }
}

export default GenericAppService;

import express from "express";

import { getRepository } from "typeorm";

import {
  BoletoFaturaSchema,
  BoletoConciliacaoSchema,
} from "./infra/data/mapping/BoletoMapping";

const app = express();

import { createConnection } from "typeorm";

createConnection();

app.get("/boleto", async (request, response) => {
  const repository = getRepository(BoletoFaturaSchema);

  const boletos = await repository.find();

  return response.json(boletos);
});

app.get("/conciliacao", async (request, response) => {
  const repository = getRepository(BoletoConciliacaoSchema);

  const boletos = await repository.find();

  return response.json(boletos);
});

app.listen(3333);
